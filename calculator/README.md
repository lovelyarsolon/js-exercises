### project specs

- [x] when a value is clicked, it should reflect at the calculator
- [x] if an operator is clicked, the operator should reflect in the calculator
- [x] if an operator is clicked, all other operators should be disabled
- [x] there should be a spaces between values and an operator e.g. ```1 + 1```
- [x] if _=_ is clicked, the result of the operation should be displayed in the calculator
- [x] if _=_ is clicked, all the values should be disabled while all the oparators are enabled
- [x] if _c_ is clicked, the calculator should reset

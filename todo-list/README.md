### project specs

- [x] if todo list is empty, it should display "my list is currently **empty**"
- [x] if todo list has only one item, it should display "there is only **1** item in my list"
- [x] if todo list has more than one items, it should display "there is only **n** item in my list", the _n_ is the count of the items (changed  to "there are **n** items in my list)
- [x] if _add_ button is clicked, the string inside the input should be added in the todo list
- [x] if _add_ button should not add anything in the todo list if the input is empty
- [x] if _Enter_ key is pressed, it should automatically add the item in the todo list without the need to click the _add_ button

// implement Vue for todoList
var todoList = new Vue({
	el : '#todo-list',
	data:{
		newTodoText: '',
		todos: [],
	},
	methods: {
	    add: function () {
	    	if(this.newTodoText){
	    		this.todos.push({
			    	text: this.newTodoText
			    })
			    this.newTodoText = ''
	    	}
	  	}
    }
})